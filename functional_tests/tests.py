import sys
from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth.models import User
from django.test import Client

class NewVisitorTest(StaticLiveServerTestCase):


	@classmethod
	def setUpClass(cls):
		for arg in sys.argv:
			if 'liveserver' in arg:
				cls.server_url = 'http://' + arg.split('=')[1]
				return
		super().setUpClass()
		cls.server_url = cls.live_server_url


	@classmethod
	def tearDownClass(cls):
		if cls.server_url == cls.live_server_url:
			super().tearDownClass()


	def setUp(self):
		self.browser = webdriver.Chrome()
		self.browser.implicitly_wait(3)


	def tearDown(self):
		self.browser.quit()

	def superuser_login_to_admin(self):
		self.client = Client()
		self.user = User.objects.create_superuser('max84', 'sergiu.iacob84@gmail.com', 'leph%t%graph')
		self.user.save()
		self.client.login(username='max84', password='leph%t%graph')
		cookie = self.client.cookies['sessionid']
		self.browser.get(self.live_server_url + '/admin/')
		self.browser.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
		self.browser.refresh()
		self.browser.get(self.live_server_url + '/admin/')

	def test_header_is_centered(self):
		# Max has heard about a cool new online app related to aerial photography. He goes
		# to check out its homepage.
		self.browser.get(self.server_url)
		# He notices the page title and header mentions "Orizon Photography"
		self.assertIn('Orizon Photography', self.browser.title)
		header_text = self.browser.find_element_by_tag_name('h1').text
		self.assertIn('Orizon Photography', header_text)
		# He also notices that the page header is nicely centered on the page.
		self.browser.set_window_size(1024, 768)
		header = self.browser.find_element_by_id('id_site_name')
		self.assertAlmostEqual(header.location['x'] + header.size['width'] / 2, 512, delta=10)
		# Satisfied, he goes back to sleep.


	def test_can_upload_a_photo(self):
		# Max has received an username and a password from the site administrator. Now
		# he can login to the admin area.
		# He sees 'Orizon Photography Admin' as the title of the page.
		self.browser.get(self.live_server_url + '/admin/')
		branding_text = self.browser.find_element_by_id('branding').text
		self.assertEqual('Orizon Photography Admin', branding_text)
		# He logs in to the admin area using the credentials he received from the site administrator.
		self.superuser_login_to_admin()
		# In the admin section of the site , Max first creates a new album for his photos.
		self.browser.find_element_by_xpath('//a[@href="/admin/omedia/album/add/"]').click()
		name_field = self.browser.find_element_by_id('id_name')
		name_field.send_keys('Football')
		description_field = self.browser.find_element_by_id('id_description')
		description_field.send_keys('Pictures from football matches')
		self.browser.find_element_by_class_name('default').click()
		# His new album now appears in the Albums list.
		self.browser.find_element_by_link_text('Football')
		self.fail('Finish the test!')