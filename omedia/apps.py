from django.apps import AppConfig

class OmediaConfig(AppConfig):
	name = "omedia"
	verbose_name = "Photos and Videos"