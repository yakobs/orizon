# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('omedia', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='album',
            name='created',
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
        migrations.AddField(
            model_name='album',
            name='description',
            field=models.TextField(default='Album Description'),
        ),
        migrations.AddField(
            model_name='album',
            name='name',
            field=models.CharField(default='Album Name', max_length=100),
        ),
    ]
