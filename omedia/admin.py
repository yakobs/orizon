from django.contrib import admin
from django.contrib.auth.models import Group
from .models import Album

admin.site.register(Album)
admin.site.unregister(Group)
