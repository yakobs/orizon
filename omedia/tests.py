from django.core.urlresolvers import resolve
from django.template.loader import render_to_string
from django.test import TestCase
from django.http import HttpRequest

from omedia.views import home_page
from omedia.models import Album

class HomePageTest(TestCase):

	def test_root_url_resolves_to_home_page_view(self):
		found = resolve('/')
		self.assertEqual(found.func, home_page)

	def test_home_page_returns_correct_html(self):
		request = HttpRequest()
		response = home_page(request)
		expected_html = render_to_string('home.html')
		self.assertEqual(response.content.decode(), expected_html)

class AlbumModelTest(TestCase):

	def test_saving_and_retrieving_albums(self):
		album1 = Album()
		album1.name = 'Olimpia Gherla - U Cluj'
		album1.description = 'Fotografii de la meciul Olimpia Gherla - U Cluj'
		album1.save()

		album2 = Album()
		album2.name = 'U Cluj - FCM Bacau'
		album2.description = 'Fotografii de la meciul U Cluj - FCM Bacau'
		album2.save()

		saved_albums = Album.objects.all()
		self.assertEqual(saved_albums.count(), 2)

		first_saved_album = saved_albums[0]
		second_saved_album = saved_albums[1]
		self.assertEqual(first_saved_album.name, 'Olimpia Gherla - U Cluj')
		self.assertEqual(first_saved_album.description, 'Fotografii de la meciul Olimpia Gherla - U Cluj')
		self.assertEqual(second_saved_album.name, 'U Cluj - FCM Bacau')
		self.assertEqual(second_saved_album.description, 'Fotografii de la meciul U Cluj - FCM Bacau')
