from django.db import models
from datetime import datetime

class Album(models.Model):
	name = models.CharField(max_length=100, default='')
	description = models.TextField(default='')
	created = models.DateTimeField(default=datetime.now)

	def __str__(self):
		return self.name
